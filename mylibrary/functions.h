#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__

#include <string>
#include "list.h"
#include "vector.h"
namespace mylib {

  void helloWorld(const std::string& words);

} // END namespace mylib

#endif // __FUNCTIONS_H__
