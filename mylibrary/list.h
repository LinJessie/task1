#ifndef LIST_H
#define LIST_H


#include <initializer_list>
#include <list>
using value_type = int;

namespace mylib {

struct Item {
 int value;
 Item *next;

};

  class List {
  public:
    using size_type  = std::size_t;

    List( std::initializer_list<value_type> list );
    List();
    size_type size() const;
    std::list<value_type>::iterator Getbegin();
    std::list<value_type>::iterator Getend();

    void PushBack(const value_type &x);
    void PushFront(const value_type &x);
    void myinsert(std::list<value_type>::const_iterator a,const value_type &x );
    void myremove(const value_type &x);
  private:
    Item root;
    Item current;
    size_type _size;
    std::list<value_type> _pri_list;
  }; // END class List

}  // END namespace mylib

#endif // LIST_H
