
#include "list.h"




namespace mylib {



  List::List(std::initializer_list<value_type> list)
    : _size {list.size()}
  {
        _pri_list = list;
  }
  List::List(){}
  std::list<value_type>::iterator List::Getbegin(){return _pri_list.begin();}
  std::list<value_type>::iterator List::Getend(){return _pri_list.end();}

  void List::PushBack(const value_type &x){ _pri_list.push_back(x);}
  void List::PushFront(const value_type &x){ _pri_list.push_front(x);}
  void List::myinsert(std::list<value_type>::const_iterator a,const value_type &x )
  {
      _pri_list.insert(a,x);
  }
  void List::myremove(const value_type &x)
  {
       _pri_list.remove(x);
  }

  List::size_type
  List::size() const { return _size; }

} // END namespace mylib

